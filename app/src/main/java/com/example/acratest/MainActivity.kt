package com.example.acratest

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import org.acra.ACRA


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        ACRA.errorReporter.handleException(CustomException("Custom throwable", Throwable("Test custom throwable")))

        println("FLAVOR: " + BuildConfig.FLAVOR)

        btnCrash.setOnClickListener {
//           untuk test application crash
            //throw RuntimeException("App Crashed")

           //ACRA.errorReporter.handleException(CustomException("Custom throwable", Throwable("Test custom throwable")))

          //untuk test application not responding
            while (true) {

            }

//            val number = "1.0".toInt()
//            println("Number : $number")
        }

    }

    class CustomException : Exception {
        constructor() : super()
        constructor(message: String) : super(message)
        constructor(message: String, cause: Throwable) : super(message, cause)
        constructor(cause: Throwable) : super(cause)
    }

}