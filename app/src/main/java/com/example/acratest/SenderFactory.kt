package com.example.acratest

import android.content.Context
import com.google.auto.service.AutoService
import org.acra.config.CoreConfiguration
import org.acra.data.StringFormat
import org.acra.sender.HttpSender
import org.acra.sender.ReportSender
import org.acra.sender.ReportSenderFactory


@AutoService(ReportSenderFactory::class)
class SenderFactory: ReportSenderFactory {
    override fun create(context: Context, config: CoreConfiguration): ReportSender {
        return CustomSender(config, HttpSender.Method.POST, StringFormat.JSON)
    }

}